#!/bin/sh

DEV=$1

for file in `find . -name vmstat.out`; do
	~/src/freebsd-vmstat-cpu.pl --file ${file} --outdir `dirname ${file}`/cpu-charts
done

for file in `find . -name iostatx.out`; do
	~/src/freebsd-iostat.pl --dev ${DEV} --file ${file} --outdir `dirname ${file}`/io-charts
done
