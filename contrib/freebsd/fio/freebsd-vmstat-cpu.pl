#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $datafile = "cpu.data";

my $file;
my $outdir;

GetOptions(
	'file:s' => \$file,
	'outdir:s' => \$outdir
);

sub usage()
{
	print "usage: $0 --file <vmstat.out> --outdir <dir>\n";
}

unless ($file and $outdir) {
	usage();
	exit(1);
}

unless (-f $file) {
	print "vmstat output file '$file' not found\n";
	exit(1);
}

if (-d $outdir) {
	print "i don't want to clobber `$outdir`, pick another location\n";
	exit(1);
}

`mkdir $outdir`;
unless (-d $outdir) {
	print "failed to make `$outdir`\n";
	exit(1);
}

open (FILE, "< $file")
	or die "cannot open file '$file'\n";

# Number of elements to trim before CPu info.
my $trim_count = 17;

# Generate data deader.
my $header = `grep '^ r' $file | head -n 1`;
chomp $header;
my @data = split / +/, $header;
splice(@data, 0, $trim_count);
my $col = 0;
my $proc = 0;
for (@data) {
    $data[$col++] = $_ . $proc;
    $proc++ if ($col % 3 == 0);
}
my $outputfile;
open ($outputfile, ">$outdir/$datafile") ||
		die "cannot open $outdir/$datafile for writing\n";
print $outputfile join(" ", "time", @data, "\n");

# Generate data.
my $x = 0;
while (<FILE>) {
	my $line = $_;
	chomp $line;
	if ($line =~ m/^ r/ or $line =~ m/^ procs/) {
		next;
	}
	@data = split / +/, $line;
	splice(@data, 0, $trim_count);
	print $outputfile join(" ", $x, @data, "\n");
	$x++;
}
close FILE;
close $outputfile;

# Dumping .r file.
my $routput;
foreach my $type (("us", "sy", "id")) {
	my $rfile = "cpu-$type.r";

	open ($routput, "> $outdir/$rfile")
		or die "cannot open file '$outdir/$rfile'\n";

	my @p_name = ();
	my $plots = "";
	for (my $i = 0; $i < $proc; $i++) {
		my $color = $i + 1;
		$plots .= "points(d\$time, d\$$type$i, type = \"o\", pch = $i, col = color[$color])\n";
		push @p_name, "\"cpu$i\"";
	}
	my $p_names = join(",", @p_name);
	chomp $plots;

	my $pch = $proc - 1;
	my $rdata = <<__RDATA__;
d <- read.table("$datafile", header = TRUE)
png(filename="cpu-$type.png", width = 1280, height = 800)
color <- rainbow($proc)
plot.default(d\$time,
	type = "n",
	main = "Processor Utilization ($type)",
	xlab = "Elapsed Time (Minutes)",
	ylab = "% Processor Utilization",
	ylim = c(0, 100))
grid()
$plots
legend(\"topleft\", c($p_names\), pch = 0:$pch, col = color)
dev.off()
__RDATA__
	print $routput $rdata;
	close $routput;
	`(cd $outdir && R --no-save < $rfile)`;
}
