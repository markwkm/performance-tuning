#!/bin/sh

if [ $# -ne 1 ]; then
	echo "usage: $0 <dir>"
	exit 1
fi

DIR=$1

for TEST in random-read random-write read-write; do
	echo "executing '${TEST}' test..."
	./run-test.sh ${TEST} ${DIR}/${TEST} 8
	rm -f /test/*
done

for TEST in seq-read seq-write; do
	echo "executing '${TEST}' test..."
	./run-test.sh ${TEST} ${DIR}/${TEST} 1
	rm -f /test/*
done
