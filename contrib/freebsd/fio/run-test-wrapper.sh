#!/bin/sh

if [ $# -ne 3 ]; then
	echo "usage: $0 <test> <outdir> <processes>"
	exit 1
fi

TEST=$1
DIR=$2
PROCS=$3

CMD=`fio --showcmd ${TEST} | sed 's/blocksize/bs/'`

COUNT=0
while [ ${COUNT} -lt ${PROCS} ]; do
	${CMD} --output ${DIR}/fio-${COUNT}.out --filename=${COUNT} &
	COUNT=$(( ${COUNT} + 1 ))
done

wait
