#!/bin/sh

if [ $# -ne 3 ]; then
	echo "usage: $0 <test-name> <dir> <processes>"
	exit 1
fi

TEST=$1
DIR=$2
PROCS=$3

if [ ! -f ${TEST} ]; then
	echo "'${TEST}' not found"
	exit 1
fi

if [ -d "${DIR}" ]; then
	echo "'${DIR}' directory for results already exists, stopping"
	exit 1
fi

mkdir -p ${DIR}

iostat -x -w 60 > ${DIR}/iostatx.out &
PID_IOSTAT=$!
vmstat -H -P -w 60 > ${DIR}/vmstat.out &
PID_VMSTAT=$!

./run-test-wrapper.sh ${TEST} ${DIR} ${PROCS}

kill ${PID_IOSTAT} ${PID_VMSTAT}
