#!/bin/sh

for file in `find . -name iostatx.data`; do
	DIR=`dirname ${file}`
	FILESD=${DIR}/iostatx-sd.data
	RFILE=${DIR}/sd.r

	head -n 1 ${file} > ${FILESD}
	tail -n 55 ${file} | head -n 50 >> ${FILESD}

	echo "d <- read.table(\"iostatx-sd.data\", header = TRUE)" > ${RFILE}
	echo "sd(d)" >> ${RFILE}
	(cd ${DIR} && R --no-save < sd.r > iosd.txt)
done
