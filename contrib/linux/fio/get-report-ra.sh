#!/bin/sh

DIR=`dirname $0`

echo "dumping..."
echo

${DIR}/get-read-results.sh seq-read

echo "short... (MB/s)"
echo

SR=`${DIR}/get-read-results.sh seq-read | tail -n 1 | cut -d ' ' -f 1`

echo ${SR}
