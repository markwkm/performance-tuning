#!/bin/sh

DIR=`dirname $0`

echo "dumping..."
echo

${DIR}/get-read-results.sh random-read
${DIR}/get-write-results.sh random-write
${DIR}/get-rw-results.sh read-write
${DIR}/get-read-results.sh seq-read
${DIR}/get-write-results.sh seq-write

echo "short... (MB/s)"
echo

RR=`${DIR}/get-read-results.sh random-read | tail -n 1 | cut -d ' ' -f 1`
RW=`${DIR}/get-write-results.sh random-write | tail -n 1 | cut -d ' ' -f 1`
MR=`${DIR}/get-rw-results.sh read-write | tail -n 3 | head -n 1 | cut -d ' ' -f 1`
MW=`${DIR}/get-rw-results.sh read-write | tail -n 1 | cut -d ' ' -f 1`
SR=`${DIR}/get-read-results.sh seq-read | tail -n 1 | cut -d ' ' -f 1`
SW=`${DIR}/get-write-results.sh seq-write | tail -n 1 | cut -d ' ' -f 1`

echo "RR RW MR MW SR SW"
echo ${RR} ${RW} ${MR} ${MW} ${SR} ${SW}
