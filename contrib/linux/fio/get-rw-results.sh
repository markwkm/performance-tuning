#!/bin/sh

DIR=$1

grep -h READ ${DIR}/fio.out | awk '{print $3}'
grep -h WRITE ${DIR}/fio.out | awk '{print $3}'

RTOTAL=0
for i in `grep -h READ ${DIR}/fio.out | awk '{print $3}' | sed 's/[^0-9]//g'`; do
	RTOTAL=$(( ${TOTAL} + ${i} ))
done
WTOTAL=0
for i in `grep -h WRITE ${DIR}/fio.out | awk '{print $3}' | sed 's/[^0-9]//g'`; do
	WTOTAL=$(( ${TOTAL} + ${i} ))
done
echo "READ: ${RTOTAL} KB/s"
echo `echo "scale=1; ${RTOTAL} / 1024" | bc` MB/s
echo "WRITE: ${WTOTAL} KB/s"
echo `echo "scale=1; ${WTOTAL} / 1024" | bc` MB/s
