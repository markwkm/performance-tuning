#!/bin/bash

for ra in seq-read-256 seq-read-512 seq-read-1024 seq-read-4096 seq-read-8192 seq-read-16384 seq-read-131072; do
	echo ${ra}
	SUMMARY=""
	for fs in `ls`; do
		if [ -d ${fs} ]; then
			#echo "${fs}"
			if [ -d "${fs}/${ra}" ]; then
				S=`../get-read-results.sh ${fs}/${ra} | awk '{print $1}'`
				SUMMARY="${SUMMARY} ${S}"
			fi
		fi
	done
	echo ${SUMMARY}
done

exit 0

for fs in `ls`; do
	if [ -d ${fs} ]; then
		echo "${fs}"
		SUMMARY=""
		for ra in seq-read-256 seq-read-512 seq-read-1024 seq-read-4096 seq-read-8192 seq-read-16384 seq-read-131072; do
			if [ -d "${fs}/${ra}" ]; then
				#echo ${ra}
				S=`../get-read-results.sh ${fs}/${ra} | awk '{print $1}'`
				SUMMARY="${SUMMARY} ${S}"
			fi
		done
		echo ${SUMMARY}
	fi
done
