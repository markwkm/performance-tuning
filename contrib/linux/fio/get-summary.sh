#!/bin/bash

for fs in `ls`; do
	if [ -d ${fs} ]; then
		if [ -d "${fs}/${test}" ]; then
			echo "${fs}"
			(cd ${fs} && ../../get-report.sh | tail -n 1) 2> /dev/null
		fi
	fi
done
