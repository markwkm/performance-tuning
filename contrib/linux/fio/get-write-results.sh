#!/bin/sh

DIR=$1

TOTAL=`grep -h WRITE ${DIR}/fio.out | awk '{print $3}' | sed 's/[^0-9]//g'`

RAW=`grep -h WRITE ${DIR}/fio.out | awk '{print $3}' | grep MiB`
if [ -n "${RAW}" ]; then
        echo "${TOTAL} MB/s"
else
        echo `echo "scale=1; ${TOTAL} / 1024" | bc` MB/s
fi
