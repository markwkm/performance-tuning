#!/bin/sh

DEV=$1

for file in `find . -name mpstat.out`; do
	~/src/linux-mpstat-cpu.pl --file ${file} --outdir `dirname ${file}`/cpu-charts
done

for file in `find . -name iostatx.out`; do
	~/src/linux-iostat.pl --dev ${DEV} --file ${file} --outdir `dirname ${file}`/io-charts
done
