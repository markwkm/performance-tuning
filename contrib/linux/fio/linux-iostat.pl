#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $datafile = "iostatx.data";

my $device;
my $file;
my $outdir;

GetOptions(
	'dev:s' => \$device,
	'file:s' => \$file,
	'outdir:s' => \$outdir
);

sub usage()
{
	print "usage: $0 --dev <cciss/c0d2> --file <iostatx.out> --outdir <dir>\n";
}

unless ($device and $file and $outdir) {
	usage();
	exit(1);
}

unless (-f $file) {
	print "iostat output file '$file' not found\n";
	exit(1);
}

if (-d $outdir) {
	print "i don't want to clobber `$outdir`, pick another location\n";
	exit(1);
}

`mkdir $outdir`;
unless (-d $outdir) {
	print "failed to make `$outdir`\n";
	exit(1);
}

open (FILE, "< $file")
	or die "cannot open file '$file'\n";

# Generate data deader.
my $header = `grep '^Device' $file | head -n 1`;
chomp $header;
$header =~ s/%//g;
$header =~ s/\//./g;
$header =~ s/-//g;
my @header = split / +/, $header;
shift @header;
my $outputfile;
open ($outputfile, ">$outdir/$datafile") ||
		die "cannot open $outdir/$datafile for writing\n";
print $outputfile join(" ", "time", @header, "\n");

# Generate data.
my $x = 0;
while (<FILE>) {
	my $line = $_;
	chomp $line;
	if ($line !~ m/^$device/) {
		next;
	}
	my @data = split / +/, $line;
	shift @data;
	print $outputfile join(" ", $x, @data, "\n");
	$x++;
}
close FILE;
close $outputfile;

# Dumping .r file.
my $routput;
foreach my $type (@header) {
	my $rfile = "iostat-$type.r";

	open ($routput, "> $outdir/$rfile")
		or die "cannot open file '$outdir/$rfile'\n";

    my $main = '';
    my $ylab = '';
    my $ylim = '';
	if ($type =~ m/^r\.s/ || $type =~ m/^w\.s/) {
		if ($type =~ m/r.s/) {
			$main = "Reads";
		} else {
			$main = "Writes";
		}
		$ylab = "Requests per Second";
	} elsif ($type =~ m/^rrqm/ || $type =~ m/wrqm/) {
		if ($type =~ m/rrqm/) {
			$main = "Reads";
		} else {
			$main = "Writes";
		}
		$ylab = "Queued Requests Merged per Second";
	} elsif ($type =~ m/rMB/ || $type =~ m/wMB/) {
		if ($type =~ m/rMB/) {
			$main = "Read Throughput";
		} else {
			$main = "Write Throughputs";
		}
		$ylab = "Megabytes per Second";
	} elsif ($type =~ m/util/) {
		$main = "Utilization";
		$ylab = "Percentage of the Time Requests were Issued";
		$ylim = ', ylim = c(0, 100)';
	} elsif ($type =~ m/svctm/) {
		$main = "Average Service Time";
		$ylab = "Milliseconds";
	} elsif ($type =~ m/await/) {
		$main = "Average Time to be Served";
		$ylab = "Milliseconds";
	} elsif ($type =~ m/avgqu/) {
		$main = "Average Queue Length";
	} elsif ($type =~ m/avgrq/) {
		$main = "Average Size of Request";
		$ylab = "Sectors";
	}

	my $rdata = <<__RDATA__;
d <- read.table("$datafile", header = TRUE)
png(filename="iostat-$type.png", width = 1280, height = 800)
color <- rainbow(1)
plot.default(d\$time,
	y = d\$$type,
	type = "o",
	col = color[1],
	pch = 0,
	main = "$main",
	xlab = "Elapsed Time (Minutes)",
	ylab = "$ylab"$ylim)
grid()
legend(\"topleft\", c("$device"), pch = 0:0, col = color)
dev.off()
__RDATA__
	print $routput $rdata;
	close $routput;
	`(cd $outdir && R --no-save < $rfile)`;
}
