#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;

my $datafile = "mpstat.data";

my $file;
my $outdir;

GetOptions(
	'file:s' => \$file,
	'outdir:s' => \$outdir
);

sub usage()
{
	print "usage: $0 --file <mpstat.out> --outdir <dir>\n";
}

unless ($file and $outdir) {
	usage();
	exit(1);
}

unless (-f $file) {
	print "vmstat output file '$file' not found\n";
	exit(1);
}

if (-d $outdir) {
	print "i don't want to clobber `$outdir`, pick another location\n";
	exit(1);
}

`mkdir $outdir`;
unless (-d $outdir) {
	print "failed to make `$outdir`\n";
	exit(1);
}

# Count lines to get the number of processors.

my $proc = 0;
open(FILE, "< $file")
	or die "cannot open file '$file'\n";
while (<FILE>) {
	my $line = $_;
	chomp $line;

	next if ($line !~ m/all/);
	while ($line = <FILE>) {
		chomp $line;
		last if ($line eq "");
		++$proc;
	}
	last;
}
close(FILE);

open(FILE, "< $file")
	or die "cannot open file '$file'\n";

# Number of elements to trim before CPu info.
my $trim_count = 2;

# Generate data deader.
my $header = `grep 'CPU' $file | head -n 1`;
chomp $header;
$header =~ s/%//g;
$header =~ s/\//./g;
my @header_data = split / +/, $header;
splice(@header_data, 0, $trim_count);
my @current = ();
# Add headers for 'all'
for (@header_data) {
	push(@current, $_ . "a")
}
# Add headers for each processor.
for (my $i = 0; $i < $proc; $i++) {
	for (@header_data) {
		push(@current, $_ . "$i")
	}
}
my $outputfile;
open ($outputfile, ">$outdir/$datafile") ||
		die "cannot open $outdir/$datafile for writing\n";
print $outputfile join(" ", "time", @current, "\n");

# Generate data.
my $x = 0;
while (<FILE>) {
	my $line = $_;
	chomp $line;
	next if ($line !~ m/CPU/);

	@current = ();
	while ($line = <FILE>) {
		chomp $line;
		last if ($line eq "");
		my @data = split / +/, $line;
		splice(@data, 0, $trim_count);
		push(@current, @data);
	}
	print $outputfile join(" ", $x, @current, "\n");
	$x++;
}
close FILE;
close $outputfile;

# Pop intr/s off so we don't plot it.
pop @header_data;

# Dumping .r file.
my $routput;
foreach my $type (@header_data) {
	my $rfile = "cpu-$type.r";

	open ($routput, "> $outdir/$rfile")
		or die "cannot open file '$outdir/$rfile'\n";

	my @p_name = ();
	my $plots = "";
	for (my $i = 0; $i < $proc; $i++) {
		my $color = $i + 1;
		$plots .= "points(d\$time, d\$$type$i, type = \"o\", pch = $i, col = color[$color])\n";
		push @p_name, "\"cpu$i\"";
	}
	my $p_names = join(",", @p_name);
	chomp $plots;

	my $pch = $proc - 1;
	my $rdata = <<__RDATA__;
d <- read.table("$datafile", header = TRUE)
png(filename="cpu-$type.png", width = 1280, height = 800)
color <- rainbow($proc)
plot.default(d\$time,
	type = "n",
	main = "Processor Utilization ($type)",
	xlab = "Elapsed Time (Minutes)",
	ylab = "% Processor Utilization",
	ylim = c(0, 100))
grid()
$plots
legend(\"topleft\", c($p_names\), pch = 0:$pch, col = color)
dev.off()
__RDATA__
	print $routput $rdata;
	close $routput;
	`(cd $outdir && R --no-save < $rfile)`;
}
