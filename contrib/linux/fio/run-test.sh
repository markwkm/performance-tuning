#!/bin/sh

if [ $# -ne 2 ]; then
	echo "usage: $0 <test-name> <dir>"
	exit 1
fi

TEST=$1
DIR=$2

if [ ! -f ${TEST} ]; then
	echo "'${TEST}' not found"
	exit 1
fi

if [ -d "${DIR}" ]; then
	echo "'${DIR}' directory for results already exists, stopping"
	exit 1
fi

mkdir -p ${DIR}

iostat -m -x 60 > ${DIR}/iostatx.out &
vmstat -n 60 > ${DIR}/vmstat.out &
sar -o ${DIR}/sar.raw 60 0 &
mpstat -P ALL 60 > ${DIR}/mpstat.out &

#CMD=`fio --showcmd ${TEST} | sed 's/blocksize/bs/'`
#${CMD} --output ${DIR}/fio.out
fio --output ${DIR}/fio.out ${TEST} 

killall iostat vmstat sar sadc mpstat
sar -A -f ${DIR}/sar.raw > ${DIR}/sar.out
