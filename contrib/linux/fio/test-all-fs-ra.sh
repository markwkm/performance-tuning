#!/bin/sh

if [ $# -ne 3 ]; then
	echo "usage: $0 <device> <testdir> <outdir>"
	exit 1
fi

DEVICE=$1
TESTDIR=$2
OUTDIR=$3

FS="ext2 ext3 ext4 jfs reiserfs xfs"

format()
{
	DEV=$1
	F=$2
	DIR=$3

	umount ${DIR}
	if [ "x${F}" = "xext2" ]; then
		mkfs.ext2 ${DEV} || exit 1
	elif [ "x${F}" = "xext3" ]; then
		mkfs.ext3 ${DEV} || exit 1
	elif [ "x${F}" = "xext4" ]; then
		mkfs.ext4 ${DEV} || exit 1
	elif [ "x${F}" = "xjfs" ]; then
		mkfs.jfs -q ${DEV} || exit 1
	elif [ "x${F}" = "xreiserfs" ]; then
		mkfs.reiserfs -f ${DEV} || exit 1
	elif [ "x${F}" = "xxfs" ]; then
		mkfs.xfs -f ${DEV} || exit 1
	else
		echo "unknown filesystem '${F}'"
		exit 1
	fi
	mount ${DEV} ${DIR}
	chmod 777 ${DIR}
}

TEST="seq-read"
for fs in $FS; do
	echo ${fs}
	format ${DEVICE} ${fs} ${TESTDIR}
	for ra in 256 512 1024 2048 4096 8192 16384 131072 262144; do
			echo "setting readahead to '${ra}' test..."
			blockdev --setra ${ra} ${DEVICE}
			./run-test.sh ${TEST} ${OUTDIR}/${fs}/${TEST}-${ra}
			rm -f /test/*
	done
done
