\documentclass{beamer}

% Colors to set everyone white on black.
\setbeamercolor{item}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}

% Title page starts here.
\title{PostgreSQL Portland\\Performance Practice Project}
\subtitle{Database Test 2 (DBT-2)\\Details}
\author{Mark Wong\\markwkm@postgresql.org}
\institute{Portland State University}
\date{February 12, 2009}

\begin{document}

\frame{\titlepage}

\frame[containsverbatim]
{
  \frametitle{Review}

  From last time:
  \begin{itemize}
    \item Series overview.
    \item DBT-2 background.
  \end{itemize}

  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Questions? )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{Contents}

  Diving into the DBT-2 test kit.

  \begin{itemize}
    \item Workload description
    \item Architecture
    \item Database Schema and Sizing
    \item Transactions
    \begin{itemize}
      \item Description
      \item SQL
    \end{itemize}
  \end{itemize}

  Note: Default kit behavior will be covered, but almost all parameters
  are changeable.
}

\frame
{
  \frametitle{Workload Description}

  \begin{itemize}
    \item Simulate wholesale supplier processing orders.  (Manage, sell,
    distribute products or services.)
    \item A basic set of operations representing a complex OLTP
    application environment.
  \end{itemize}

  For example, someone at a terminal (in no particular order):
  \begin{itemize}
    \item checks the stock levels in the warehouse.
    \item enters a new order for items the supplier sells.
    \item searches for the status of an order.
    \item enters payment for an order.
    \item processes an order for delivery.
  \end{itemize}
}

\frame
{
  \frametitle{DBT-2 Components}

  \begin{center}
    \pgfimage[height=1.75cm]{dbt2-component-1}\\
    \vspace{1em}
    \pgfimage[height=1.75cm]{dbt2-component-2}\\
    \vspace{1em}
    \pgfimage[height=1.75cm]{dbt2-component-3}
  \end{center}
}

\frame
{
  \frametitle{Database Schema Diagram}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-schema}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Database Schema Notes}

  \begin{itemize}
    \item 9 Tables
    \item No foreign key constraints
  \end{itemize}

  \color{cyan}
  \begin{verbatim}
          __     __         /                      \
         /  \~~~/  \  . o O | Is this a good schema |
   ,----(     oo    )       | design?               |
  /      \__     __/        \                      /
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{Scale Factor}

  \begin{itemize}
    \item Determines database size.
    \item Determines number of users driving the workload.
  \end{itemize}
}

\frame
{
  \frametitle{Scale Factor's effect on database row counts}

  Let \texttt{W} be the scale factor, which determines the starting size
  of the database\footnote{TPC-C Benchmark Specification v5.9 Clause
  1.2.1}:

  \begin{itemize}
    \item \texttt{warehouse} has \texttt{W} warehouses
    \item \texttt{district} has 10 districts per warehouse
    \item \texttt{customer} has 3,000 customers per district
    \begin{itemize}
       \item \texttt{history} has about a row per customer
       \item \texttt{orders} has about a row per customer
      \begin{itemize}
       \item \texttt{order\_line} has 5-15 items per order
       \item About 30\% of the orders are new in \texttt{new\_order}
      \end{itemize}
    \end{itemize}
    \item \texttt{item} is fixed with 100,000 items in the company
    \item \texttt{stock} stocks 100,000 items per warehouse
  \end{itemize}
}

\frame
{
  \frametitle{Scale Factor's effect on database table size}

  Numbers are in 1,000 bytes per Warehouse in the database
  \footnote{TPC-C Benchmark Specification v5.9 Clause 4.2.2}:
  \begin{itemize}
    \item \texttt{warehouse} - 0.089
    \item \texttt{district} - 0.950
    \item \texttt{customer} - 19,650
    \item \texttt{history} - 1,380
    \item \texttt{orders} - 720
    \item \texttt{order\_line} - 16,200
    \item \texttt{new\_order} - 72
    \item \texttt{item} - 8,200
    \item \texttt{stock} - 30,600
  \end{itemize}
}

\frame
{
  \frametitle{Scale Factor's effect on table row size}

  Numbers are in bytes per Warehouse in the database \footnote{TPC-C
  Benchmark Specification v5.9 Clause 4.2.2}:
  \begin{itemize}
    \item \texttt{warehouse} - 89
    \item \texttt{district} - 95
    \item \texttt{customer} - 655
    \item \texttt{history} - 146
    \item \texttt{orders} - 24
    \item \texttt{order\_line} - 54
    \item \texttt{new\_order} - 8
    \item \texttt{item} - 82
    \item \texttt{stock} - 306
  \end{itemize}
}

\frame
{
  \frametitle{DBT-2 Transactions and the Mix}

  Now descriptions of the 5 transactions and the SQL statements executed
  in each transaction.  Transaction mix (percentage of how often a
  transaction is called also shown here.
  \begin{itemize}
    \item \color{violet}Delivery \color{white}-
		  \color{cyan}Read\color{white}/\color{red}Write \color{white}-
		  4\%
    \item \color{violet}New Order \color{white}-
		  \color{cyan}Read\color{white}/\color{red}Write \color{white}-
		  55\%
    \item \color{cyan}Order Status \color{white}- \color{cyan}Read
          \color{white}Only - 4\%
    \item \color{violet}Payment \color{white}-
		  \color{cyan}Read\color{white}/\color{red}Write \color{white}-
		  43\%
    \item \color{cyan}Stock Level \color{white}- \color{cyan}Read
          \color{white}Only - 4\%
  \end{itemize}

  Note: Color coded in an attempted to match upcoming charts.
  \begin{itemize}
    \item \color{cyan} Read
    \item \color{red} Write
    \item \color{violet} Read/Write
  \end{itemize}
}

\frame
{
  \frametitle{Delivery Transaction Description}

  \begin{quotation}
  The Delivery business transaction consists of processing a batch of 10
  new (not yet delivered) orders. Each order is processed (delivered) in
  full within the scope of a read-write database transaction. The number
  of orders delivered as a group (or batched) within the same database
  transaction is implementation specific. The business transaction,
  comprised of one or more (up to 10) database transactions, has a low
  frequency of execution and must complete within a relaxed response
  time requirement.

  The Delivery transaction is intended to be executed in deferred mode
  through a queuing mechanism, rather than interactively, with terminal
  response indicating transaction completion. The result of the deferred
  execution is recorded into a result file.\footnote{TPC-C Benchmark
  Specification v5.9 Clause 2.7}
  \end{quotation}
}

\frame
{
  \frametitle{Delivery Table Touches}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-delivery}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Delivery SQL Statements}

  For every district in the table:

  \begin{columns}[t]
  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
SELECT no_o_id
FROM new_order
WHERE no_w_id = %d
  AND no_d_id = %d

DELETE FROM new_order
WHERE no_o_id = %s
  AND no_w_id = %d
  AND no_d_id = %d

SELECT o_c_id
FROM orders
WHERE o_id = %s
  AND o_w_id = %d
  AND o_d_id = %d

UPDATE orders
SET o_carrier_id = %d
WHERE o_id = %s
  AND o_w_id = %d
  AND o_d\_id = %d
    \end{verbatim}

  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
UPDATE order_line
SET ol_delivery_d = current_timestamp
WHERE ol_o_id = %s
  AND ol_w_id = %d
  AND ol_d_id = %d

SELECT SUM(ol_amount * ol_quantity)
FROM order_line
WHERE ol_o_id = %s
  AND ol_w_id = %d
  AND ol_d_id = %d

UPDATE customer
SET c_delivery_cnt = c_delivery_cnt + 1,
    c_balance = c_balance + %s
WHERE c_id = %s
  AND c_w_id = %d
  AND c_d_id = %d
    \end{verbatim}
  \end{columns}
}

\frame
{
  \frametitle{New Order Transaction Description}

  \begin{quote}
  The New-Order business transaction consists of entering a complete
  order through a single database transaction. It represents a
  mid-weight, read-write transaction with a high frequency of execution
  and stringent response time requirements to satisfy on-line users.
  This transaction is the backbone of the workload. It is designed to
  place a variable load on the system to reflect on-line database
  activity as typically found in production environments.\footnote{TPC-C
  Benchmark Specification v5.9 Clause 2.4}
  \end{quote}
}

\frame
{
  \frametitle{New Order Table Touches}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-new-order}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{New Order SQL Statements}

  \begin{columns}[t]
  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
SELECT w_tax
FROM warehouse
WHERE w_id = %d

SELECT d_tax, d_next_o_id
FROM district 
WHERE d_w_id = %d
  AND d_id = %d
FOR UPDATE

UPDATE district
SET d_next_o_id = d_next_o_id + 1
WHERE d_w_id = %d
  AND d_id = %d

SELECT c_discount, c_last, c_credit
FROM customer
WHERE c_w_id = %d
  AND c_d_id = %d
  AND c_id = %d

INSERT INTO new_order (no_o_id, no_w_id, no_d_id)
VALUES (%s, %d, %d)

INSERT INTO orders (o_id, o_d_id, o_w_id, o_c_id,
                    o_entry_d, o_carrier_id,
                    o_ol_cnt, o_all_local)
VALUES (%s, %d, %d, %d, current_timestamp, NULL,
        %d, %d)
    \end{verbatim}
  \column{.5\textwidth}
    For 10 to 15 items (1\% of transaction fail here causing a
\texttt{ROLLBACK}):
    \tiny
    \begin{verbatim}
SELECT i_price, i_name, i_data
FROM item
WHERE i_id = %d

SELECT s_quantity, %s, s_data
FROM stock
WHERE s_i_id = %d
  AND s_w_id = %d

UPDATE stock
SET s_quantity = s_quantity - %d
WHERE s_i_id = %d
  AND s_w_id = %d

INSERT INTO order_line (l_o_id, ol_d_id, ol_w_id,
                        ol_number, ol_i_id,
                        ol_supply_w_id,
                        ol_delivery_d,
                        ol_quantity,
                        ol_amount, ol_dist_info)
VALUES (%s, %d, %d, %d, %d, %d, NULL, %d, %f,
        '\%s')
    \end{verbatim}
  \end{columns}
}

\frame
{
  \frametitle{Order Status Transaction Description}

  \begin{quote}
  The Order-Status business transaction queries the status of a
  customer's last order. It represents a mid-weight read-only database
  transaction with a low frequency of execution and response time
  requirement to satisfy on-line users. In addition, this table includes
  non-primary key access to the CUSTOMER table.\footnote{TPC-C Benchmark
  Specification v5.9 Clause 2.6}
  \end{quote}
}

\frame
{
  \frametitle{Order Status Table Touches}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-order-status}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Order Status SQL Statements}

  Get the customer ID (c\_id) only if it is not already known.

  \begin{columns}[t]
  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
SELECT c_id
FROM customer
WHERE c_w_id = %d
  AND c_d_id = %d
  AND c_last = '%s'
ORDER BY c_first ASC

SELECT c_first, c_middle, c_last, c_balance
FROM customer
WHERE c_w_id = %d
  AND c_d_id = %d
  AND c_id = %d

SELECT o_id, o_carrier_id, o_entry_d, o_ol_cnt
FROM orders
WHERE o_w_id = %d
  AND o_d_id = %d
  AND o_c_id = %d
ORDER BY o_id DESC
    \end{verbatim}
  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
SELECT ol_i_id, ol_supply_w_id, ol_quantity,
       ol_amount, ol_delivery_d
FROM order_line
WHERE ol_w_id = %d
  AND ol_d_id = %d
  AND ol_o_id = %s
    \end{verbatim}
  \end{columns}
}

\frame
{
  \frametitle{Payment Transaction Description}

  \begin{quote}
  The Payment business transaction updates the customer's balance and
  reflects the payment on the district and warehouse sales statistics.
  It represents a light-weight, read-write transaction with a high
  frequency of execution and stringent response time requirements to
  satisfy on-line users. In addition, this transaction includes
  non-primary key access to the CUSTOMER table.\footnote{TPC-C Benchmark
  Specification v5.9 Clause 2.5}
  \end{quote}
}

\frame
{
  \frametitle{Payment Table Touches}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-payment}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Payment SQL Statements (part 1/2)}

  \begin{columns}[t]
  \column{.5\textwidth}
    \tiny
    \begin{verbatim}
SELECT w_name, w_street_1, w_street_2, w_city,
       w_state, w_zip
FROM warehouse
WHERE w_id = %d

UPDATE warehouse
SET w_ytd = w_ytd + %f
WHERE w_id = %d

SELECT d_name, d_street_1, d_street_2, d_city,
       d_state, d_zip
FROM district
WHERE d_id = %d
  AND d_w_id = %d

UPDATE district
SET d_ytd = d_ytd + %f
WHERE d_id = %d
  AND d_w_id = %d
    \end{verbatim}
  \column{.5\textwidth}
    Get the customer ID (c\_id) if not already known.
    \tiny
    \begin{verbatim}
SELECT c_id
FROM customer
WHERE c_w_id = %d
  AND c_d_id = %d
  AND c_last = '%s'
ORDER BY c_first ASC

SELECT c_first, c_middle, c_last, c_street_1,
       c_street_2, c_city, c_state, c_zip,
       c_phone, c_since, c_credit, c_credit_lim,
       c_discount, c_balance, c_data,
       c_ytd_payment
FROM customer
WHERE c_w_id = %d
  AND c_d_id = %d
  AND c_id = %d
    \end{verbatim}
  \end{columns}
}

\frame[containsverbatim]
{
  \frametitle{Payment SQL Statements (part 2/2)}

  If the customer has good credit:
  \tiny
  \begin{verbatim}
UPDATE customer
SET c_balance = c_balance - %f,
    c_ytd_payment = c_ytd_payment + 1
WHERE c_id = %d
  AND c_w_id = %d
  AND c_d_id = %d
  \end{verbatim}

  \normalsize
  Otherwise if the customer has bad credit:
  \tiny
  \begin{verbatim}
UPDATE customer
SET c_balance = c_balance - %f,
    c_ytd_payment = c_ytd_payment + 1,
    c_data = E'%s'
WHERE c_id = %d
  AND c_w_id = %d
  AND c_d_id = %d

INSERT INTO history (h_c_id, h_c_d_id, h_c_w_id,
                     h_d_id, h_w_id, h_date,
                     h_amount, h_data)
VALUES (%d, %d, %d, %d, %d, current_timestamp,
        %f, E'%s    %s')
  \end{verbatim}
}

\frame
{
  \frametitle{Stock Level Transaction Description}

  \begin{quote}
  The Stock-Level business transaction determines the number of recently
  sold items that have a stock level below a specified threshold. It
  represents a heavy read-only database transaction with a low frequency
  of execution, a relaxed response time requirement, and relaxed
  consistency requirements.\footnote{TPC-C Benchmark Specification v5.9
  Clause 2.8}
  \end{quote}
}

\frame
{
  \frametitle{Stock Level Table Touches}

  \begin{center}
    \pgfimage[height=7cm]{dbt2-stock-level}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Stock Level SQL Statements}

  \tiny
  \begin{verbatim}
SELECT d_next_o_id
FROM district
WHERE d_w_id = %d
  AND d_id = %d

SELECT count(*)
FROM order_line, stock, district
WHERE d_id = %d
  AND d_w_id = %d
  AND d_id = ol_d_id
  AND d_w_id = ol_w_id
  AND ol_i_id = s_i_id
  AND ol_w_id = s_w_id
  AND s_quantity < %d
  AND ol_o_id BETWEEN (%d) AND (%d)
  \end{verbatim}
}

\frame
{
  \frametitle{Stressing the database and the Transaction Mix}

  The system is stressed by a C program that creates a thread per
  district, per warehouse.  In other words, a database built with a
  scale factor of 10 will have 10 warehouses.  Therefore 100 users will
  be simulated to represent a distinct person working for each of the 10
  districts for each of the 10 warehouses.  Each user executes
  transaction to the specified approximate mix:
}

\frame[containsverbatim]
{
  \frametitle{Thinking and Keying Time}

  \begin{columns}[t]
  \column{.5\textwidth}
    Thinking Time (How long it take to decide what to enter next,
    negative exponential function.)
    \begin{itemize}
      \item Delivery - 5 seconds
      \item New Order - 12 seconds
      \item Order Status - 10 seconds
      \item Payment - 12 seconds
      \item Stock Level - 5 seconds
    \end{itemize}
  \column{.5\textwidth}
    Keying Time (How long it takes to enter data, fixed.)
    \begin{itemize}
      \item Delivery - 5 seconds
      \item New Order - 18 seconds
      \item Order Status - 2 seconds
      \item Payment - 3 seconds
      \item Stock Level - 2 seconds
    \end{itemize}
  \end{columns}

  \color{cyan}
  \begin{verbatim}
          __     __         /                        \
         /  \~~~/  \  . o O | Can I get a jumbo sized |
   ,----(     oo    )       | terminal?               |
  /      \__     __/        \                        /
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Comments?  Questions? )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{Materials Are Freely Available}

  PDF
  \begin{itemize}
  \item \url{http://www.slideshare.net/markwkm}
  \end{itemize}

  \LaTeX{}~Beamer (source)
  \begin{itemize}
  \item \footnotesize
        \url{http://git.postgresql.org/?p=~markwkm/performance-tuning.git}
  \end{itemize}
}

\frame
{
  \frametitle{Time and Location}

  When: 2nd Thursday of the month\\
  Location: Portland State University\\
  Room: FAB 86-01 (Fourth Avenue Building)\\
  Map: \url{http://www.pdx.edu/map.html}
}

\frame[containsverbatim]
{
  \frametitle{Coming up next time\ldots}

  How to run DBT-2 run the kit.

  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Thank you! )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Acknowledgements}

  Haley Jane Wakenshaw\\
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \ 
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{License}

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License. To view a copy of this license, (a) visit
  \url{http://creativecommons.org/licenses/by/3.0/us/}; or, (b) send a
  letter to Creative Commons, 171 2nd Street, Suite 300, San
  Francisco, California, 94105, USA.
}

\end{document}
