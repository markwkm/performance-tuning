\documentclass{beamer}

% Colors to set everyone white on black.
\setbeamercolor{item}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}

% Title page starts here.
\title{PostgreSQL Portland\\Performance Practice Project}
\subtitle{Database Test 2 (DBT-2)\\How to Use the Kit}
\author{Mark Wong\\markwkm@postgresql.org}
\institute{Portland State University}
\date{March 12, 2009}

\begin{document}

\frame{\titlepage}

\frame
{
  \frametitle{How to use the DBT-2 test kit}

  Further questions regarding the test kit can be sent to the mailing
  list: osdldbt-general@lists.sourceforge.net

  You can sign up for the mailing list here:
  \footnotesize\url{http://lists.sourceforge.net/mailman/listinfo/osdldbt-general}
}

\frame
{
  \frametitle{\color{red}WARNING}

  This test kit is a work in progress.  The last ``release'' was
  February 11, 2007.  The next ``release'' still needs a bit of work
  before it's polished.  So let's work with the source repository and
  hope it still works with these instructions.
}

\frame
{
  \frametitle{Prerequisites for building}

  \begin{itemize}
    \item \texttt{PostgreSQL} must be installed.
    \item \texttt{PostgreSQL} executables must be in your path.
    \item \texttt{cmake} must be installed, used to build the kit.
    \item A C compiler.
  \end{itemize}
}

\frame
{
  \frametitle{Recommended for running}

  None of these are required for running, but help characterize
  different aspects of the system.
  \begin{itemize}
    \item \texttt{Test::Parser} perl module for calculating test
          results.
    \item \texttt{systat}, \texttt{procps}, and \texttt{iopp} for
          collecting system statistics.
    \item \texttt{readprofile} for Linux kernel profiling.
    \item \texttt{oprofile} for additional Linux kernel profiling, and
          user space profiling, call graphs, and source code and
          assembly annotations.
  \end{itemize}
}

\frame
{
  \frametitle{Prerequisites for analyzing}

  \begin{itemize}
    \item \texttt{gnuplot} to for making charts.
    \item \texttt{Test::Presenter} perl module for the modules that
          produce the charts.
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Get the kit}

  \small
  \begin{verbatim}
git clone git://git.postgresql.org/git/~markwkm/dbt2.git
  \end{verbatim}

  \texttt{git} is not required, you can download a tarball by clicking on the
  \texttt{snapshot} link from here:
  \small\url{http://git.postgresql.org/?p=~markwkm/dbt2.git;a=summary}
}

\frame[containsverbatim]
{
  \frametitle{Relevant documentation}

  Documentation to supplement or contradict what is in this
  howto presentation.

  Files to note:
  \begin{itemize}
    \item INSTALL
    \item README
    \item README-POSTGRESQL
  \end{itemize}

  \color{red}WARNING\color{white}: Documentation may be incorrect.
}

\frame[containsverbatim]
{
  \frametitle{Build and Install the Kit}

  \begin{verbatim}
cmake CMakeLists.txt
make
  \end{verbatim}

  Note: The CMakeLists.txt file is not very polished.  The kit will not
  build properly if both MySQL and PostgreSQL are in your path.

  \begin{verbatim}
make install DESTDIR=/usr/local
  \end{verbatim}

  Note: Don't fear, you can uninstall the kit:
  \begin{verbatim}
xargs rm < install_manifest.txt
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Install supporting perl modules}

  Install from cpan:
  \footnotesize
  \begin{verbatim}
cpan install Test::Parser
cpan install Test::Presenter
  \end{verbatim}

  \normalsize
  Install from source\footnote{You can use \texttt{subversion} or
  \texttt{git} to check out the source code.}:
  \footnotesize
  \begin{verbatim}
svn co https://svn.sourceforge.net/svnroot/crucible/Test-Parser
cd Test-Parser
perl Makefile.PL
make
make install

svn co https://svn.sourceforge.net/svnroot/crucible/Test-Presenter
cd Test-Presenter
perl Makefile.PL
make
make install
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Build the C stored functions}

  \begin{verbatim}
cd storedproc/pgsql/c
make
make install
  \end{verbatim}

  Note: The \texttt{make install} command needs to be run as the owner
  of the PostgreSQL installation, and the PostgreSQL installation to be
  used must in the path.
}

\frame[containsverbatim]
{
  \frametitle{Setting up the user environment}

  \begin{verbatim}
export DBNAME=dbt2  # Defines the database name to use.
export PGDATA=/var/lib/data  # Defines where the
                             # database will be built.
export DBT2TSDIR=/mnt/dbt2  # Defines where additional
                            # mount points are for use
                            # with tables spaces.
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Build the database}

  Build a 1 warehouse database, generating the load file in
  \texttt{/tmp/datafile}:
  \begin{verbatim}
dbt2-pgsql-build-db -d /tmp/datafile -g -w 1
  \end{verbatim}

  Again but use the \texttt{-b} flag to load the tables and build the
  indexes in parallel: \texttt{/tmp/datafile}.
  \begin{verbatim}
dbt2-pgsql-build-db -d /tmp/datafile -g -w 1 -b
  \end{verbatim}

  Yet again but use the \texttt{-t} flag to use the predefined location
  specified to use tablespaces:
  \begin{verbatim}
dbt2-pgsql-build-db -d /tmp/datafile -g -w 1 -b -t
  \end{verbatim}

  Note: If you have a database built already, the \texttt{-r} flag is
  need to drop the database first.  Also if data files are already
  created, drop the use of the \texttt{-g} flag so they are not
  generated again.
}

\frame[containsverbatim]
{
  \frametitle{Tablespace mount points for tables}

  In the \texttt{\$\{DBT2TSDIR\}} directory, disk devices can be mounted
  at the following points:
  \begin{itemize}
    \item customer - The \texttt{customer} table.
    \item district - The \texttt{district} table.
    \item history - The \texttt{history} table.
    \item item - The \texttt{item} table.
    \item new\_order - The \texttt{new order} table.
    \item order\_line - The \texttt{order line} table.
    \item orders - The \texttt{orders} table.
    \item stock - The \texttt{stock} table.
    \item warehouse - The \texttt{warehouse} table.
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Tablespace mount points for indexes}

  In the \texttt{\$\{DBT2TSDIR\}} directory, disk devices can be mounted
  at the following points for the primary key indexes:
  \begin{itemize}
    \item pk\_customer - The \texttt{customer} table primary key index.
    \item pk\_district - The \texttt{district} table primary key index.
    \item pk\_item - The \texttt{item} table primary key index.
    \item pk\_new\_order - The \texttt{new order} table primary key index.
    \item pk\_order\_line - The \texttt{order line} table primary key index.
    \item pk\_orders - The \texttt{orders} table primary key index.
    \item pk\_stock - The \texttt{stock} table primary key index.
    \item pk\_warehouse - The \texttt{warehouse} table primary key index.
  \end{itemize}

  For the rest of the indexes:
  \begin{itemize}
    \item index1 - An index on the \texttt{orders} table.
    \item index2 - An index on the \texttt{customer} table.
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Run a test}

  Run a 2 minute test (120 seconds) against a 1 warehouse database and
  save the test results to \texttt{/tmp/test}.
  \begin{verbatim}
dbt2-run-workload -a pgsql -d 120 -w 1 -o /tmp/test
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Results!}

  If the \texttt{Test::Parser} perl module is install and the test ran
  successfully you'll see results formatted something like:
  \tiny
  \begin{verbatim}
                         Response Time (s)
 Transaction      %    Average :    90th %        Total        Rollbacks      %
------------  -----  ---------------------  -----------  ---------------  -----
    Delivery   3.97     11.886 :    13.391        44959                0   0.00
   New Order  45.34     10.673 :    11.887       513109             5054   0.99
Order Status   4.01     10.408 :    11.667        45409                0   0.00
     Payment  42.66     10.396 :    11.688       482790                0   0.00
 Stock Level   4.01     10.278 :    11.539        45352                0   0.00
------------  -----  ---------------------  -----------  ---------------  -----

8482.31 new-order transactions per minute (NOTPM)
59.3 minute duration
0 total unknown errors
1041 second(s) ramping up
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Generating a report}

  Generate a report with charts\footnote{\texttt{Test::Presenter} and
  \texttt{gnuplot} must be installed.}:
  \begin{verbatim}
dbt2-generate-report --indir /tmp/test \
                     --outdir /tmp/test/report
  \end{verbatim}

  An \texttt{index.html} file will be created in
  \texttt{/tmp/test/report/index.html}
}

\frame[containsverbatim]
{
  \frametitle{Generating a report with tablespaces}

  To determine how to name your devices in the comma separated list for
  each flag, look to see how the devices are named by iostat.  For
  example:

  \begin{verbatim}
dbt2-generate-report --indir /tmp/test \
                     --outdir /tmp/test/report
                     --log cciss/c0d4,cciss/c0d5
  \end{verbatim}

  Transaction log device:
  \begin{itemize}
    \item \texttt{--log}
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Flags for plotting specific i/o data for tables}

  Table devices:
  \begin{itemize}
    \item \texttt{--customer}
    \item \texttt{--district}
    \item \texttt{--history}
    \item \texttt{--item}
    \item \texttt{--new\_order}
    \item \texttt{--order\_line}
    \item \texttt{--orders}
    \item \texttt{--stock}
    \item \texttt{--warehouse}
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Flags for plotting specific i/o data for indexes}

  Primary key index devices:
  \begin{itemize}
    \item \texttt{--pkcustomer}
    \item \texttt{--pkdistrict}
    \item \texttt{--pkitem}
    \item \texttt{--pknew\_order}
    \item \texttt{--pkorder\_line}
    \item \texttt{--pkorders}
    \item \texttt{--pkstock}
    \item \texttt{--pkwarehouse}
  \end{itemize}

  Other indexes device:
  \begin{itemize}
    \item \texttt{--index1}
    \item \texttt{--index2}
  \end{itemize}
}

\frame
{
  \frametitle{Materials Are Freely Available}

  PDF
  \begin{itemize}
    \item \url{http://www.slideshare.net/markwkm}
  \end{itemize}

  \LaTeX{}~Beamer (source)
  \begin{itemize}
    \item \footnotesize
          \url{http://git.postgresql.org/?p=~markwkm/performance-tuning.git}
  \end{itemize}
}

\frame
{
  \frametitle{Time and Location}

  When: 2nd Thursday of the month\\
  Location: Portland State University\\
  Room: FAB 86-01 (Fourth Avenue Building)\\
  Map: \url{http://www.pdx.edu/map.html}
}

\frame[containsverbatim]
{
  \frametitle{Coming up next time\ldots}

  Linux file system testing using
  fio\footnote{\url{http://brick.kernel.dk/snaps/}}.

  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Thank you! )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Acknowledgements}

  Haley Jane Wakenshaw\\
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \ 
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{License}

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License. To view a copy of this license, (a) visit
  \url{http://creativecommons.org/licenses/by/3.0/us/}; or, (b) send a
  letter to Creative Commons, 171 2nd Street, Suite 300, San
  Francisco, California, 94105, USA.
}

\end{document}
