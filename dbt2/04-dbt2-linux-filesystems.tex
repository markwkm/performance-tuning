\documentclass{beamer}

% Colors to set everyone white on black.
\setbeamercolor{item}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}

% Title page starts here.
\title{PostgreSQL Portland\\Performance Practice Project}
\subtitle{Database Test 2 (DBT-2)\\Characterizing Filesystems}
\author{Mark Wong\\markwkm@postgresql.org}
\institute{Portland State University}
\date{April 9, 2009}

\begin{document}

\frame{\titlepage}

\frame[containsverbatim]
{
  \frametitle{Review}

  From last time:
  \begin{itemize}
    \item DBT-2 live demo.
  \end{itemize}

  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Questions? )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \begin{columns}[c]
    \column{.5\textwidth}
    This is:
    \begin{itemize}
      \item A collection of reason why you should test your own system.
      \item A guide for how to understand the i/o behavior of your own system.
      \item A model of simple i/o behavior based on PostgreSQL
    \end{itemize}
    \column{.5\textwidth}
    This is not:
    \begin{itemize}
      \item A comprehensive Linux filesystem evaluation.
      \item A filesystem tuning guide (mkfs and mount options).
	  \item Representative of your system (unless you have the exact
            same hardware.)
      \item A test of reliability or durability.
    \end{itemize}
  \end{columns}
}

\frame
{
  \frametitle{Contents}

  \begin{itemize}
    \item Criteria
    \item Preconceptions
    \item fio Parameters
    \item Testing
    \item Test Results
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Some hardware details}

  \begin{itemize}
    \item HP DL380 G5
    \item 2 x Quad Core Intel(R) Xeon(R) E5405
    \item HP 32GB Fully Buffered DIMM PC2-5300 8x4GB DR LP Memory
    \item HP Smart Array P800 Controller
    \item MSA70 with 25 x HP 72GB Hot Plug 2.5 SAS 15,000 rpm Hard
          Drives
  \end{itemize}

  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Thank you HP! )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{Criteria}

  PostgreSQL dictates the following (even though some of them are
  actually configurable:
  \begin{itemize}
    \item 8KB block size
    \item No use of posix\_fadvise
    \item No use of direct i/o
    \item No use of async i/o
    \item Uses processes, not threads.
  \end{itemize}

  Based on the hardware we determine these criteria:
  \begin{itemize}
    \item 64GB data set - To minimize any caching in the 32GB of system memory.
    \item 8 processes performing i/o - One fio process per core.
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Linux Filesystems Tested}

  \begin{itemize}
    \item ext2
    \item ext3
    \item ext4
    \item jfs
    \item reiserfs
    \item xfs
  \end{itemize}

  \color{yellow}
  \tiny
  \begin{verbatim}
        a8888b. . o O ( What is your favorite filesystem? )
       d888888b.
       8P"YP"Y88
       8|o||o|88
       8'    .88
       8`._.' Y8.
      d/      `8b.
    .dP   .     Y8b.
   d8:'   "   `::88b.
  d8"           `Y88b
 :8P     '       :888
  8a.    :      _a88P
._/"Yaa_ :    .| 88P|
\    YP"      `| 8P  `.
/     \._____.d|    .'
`--..__)888888P`._.'
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Hardware RAID Configurations}

  \begin{itemize}
    \item RAID 0
    \item RAID 1
    \item RAID 1+0
    \item RAID 5
    \item RAID 6
  \end{itemize}

  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Which RAID would you use? )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Preconceptions}

  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Everyone gets to participate. )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( atime slows us down a lot. )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Partition alignment is huge. )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __         /                            \
         /  \~~~/  \  . o O | Doubling your disks doubles |
   ,----(     oo    )       | your performance.           |
  /      \__     __/        \                            /
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( RAID-5 is awful at writes. )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( RAID-6 improves on RAID-5. )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __         /                           \
         /  \~~~/  \  . o O | Journaling filesystems are |
   ,----(     oo    )       | the slowest.               |
  /      \__     __/        \                           /
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{fio\footnote{\url{http://brick.kernel.dk/snaps/}}}

  Flexible I/O by Jens Axboe.
  \begin{itemize}
    \item Sequential Read
    \item Sequential Write
    \item Random Read
    \item Random Write
    \item Random Read/Write
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Sequential Read}

  \begin{verbatim}
[seq-read]
rw=read
size=8g
directory=/test
fadvise_hint=0
blocksize=8k
direct=0
numjobs=8
nrfiles=1
runtime=1h
exec_prerun=echo 3 > /proc/sys/vm/drop_caches
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Sequential Write}

  \begin{verbatim}
[seq-write]
rw=write
size=8g
directory=/test
fadvise_hint=0
blocksize=8k
direct=0
numjobs=8
nrfiles=1
runtime=1h
exec_prerun=echo 3 > /proc/sys/vm/drop_caches
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Random Read}

  \begin{verbatim}
[random-read]
rw=randread
size=8g
directory=/test
fadvise_hint=0
blocksize=8k
direct=0
numjobs=8
nrfiles=1
runtime=1h
exec_prerun=echo 3 > /proc/sys/vm/drop_caches
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Random Write}

  \begin{verbatim}
[random-write]
rw=randwrite
size=8g
directory=/test
fadvise_hint=0
blocksize=8k
direct=0
numjobs=8
nrfiles=1
runtime=1h
exec_prerun=echo 3 > /proc/sys/vm/drop_caches
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Random Read/Write}

  \begin{verbatim}
[read-write]
rw=rw
rwmixread=50
size=8g
directory=/test
fadvise_hint=0
blocksize=8k
direct=0
numjobs=8
nrfiles=1
runtime=1h
exec_prerun=echo 3 > /proc/sys/vm/drop_caches
  \end{verbatim}
}

\frame
{
  \frametitle{Versions Tested}

  Linux 2.6.25-gentoo-r6

  fio v1.22

  and

  Linux 2.6.28-gentoo

  fio v1.23
}

\frame[containsverbatim]
{
  \small
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Results! )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}

  \color{white}
  There is more information than what is in this presentation, raw data
  is at:
  \url{http://207.173.203.223/~markwkm/community6/fio/2.6.28/}

  For example other things of interest may be:
  \begin{itemize}
    \item Number of i/o operations.
    \item Processor utilization.
  \end{itemize}
}

\frame
{
  \frametitle{A few words about statistics...}

  Not enough data to calculate good statistics.

  Examine the following charts for a feel of how reliable the results
  may be.
}

\frame
{
  \frametitle{Random Read with Linux 2.6.28-gentoo ext2 on 1 Disk}

  \begin{center}
    \pgfimage[width=1\textwidth]{rr-iostat-kb}
  \end{center}
}

\frame
{
  \frametitle{Random Write with Linux 2.6.28-gentoo ext2 on 1 Disk}

  \begin{center}
    \pgfimage[width=1\textwidth]{rw-iostat-kb}
  \end{center}
}

\frame
{
  \frametitle{Read/Write Mix with Linux 2.6.28-gentoo ext2 on 1 Disk}

  \begin{center}
    \pgfimage[width=1\textwidth]{mix-iostat-kb}
  \end{center}
}

\frame
{
  \frametitle{Sequential Read with Linux 2.6.28-gentoo ext2 on 1 Disk}

  \begin{center}
    \pgfimage[width=1\textwidth]{sr-iostat-kb}
  \end{center}
}

\frame
{
  \frametitle{Sequential Write with Linux 2.6.28-gentoo ext2 on 1 Disk}

  \begin{center}
    \pgfimage[width=1\textwidth]{sw-iostat-kb}
  \end{center}
}

\frame
{
  Results for all the filesystems when using only a single disk...

  (If you see missing data, it means the test didn't complete.)
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{1-disk-raid0}
  \end{center}
}

\frame
{
  Results for all the filesystems when striping across four disks...
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-raid0}
  \end{center}
}

\frame
{
  Let's look at that step by step for RAID 0.
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-ext2}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-ext3}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-ext4}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-jfs}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-reiserfs}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-xfs}
  \end{center}
}

\frame
{
  Note trends might not be the same when adding disks to other RAID
  configurations.
}

\frame
{
  What benefits are there in mirroring?
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{mirror-ext2}
  \end{center}
}

\frame
{
  \begin{itemize}
  \item Should have same write performance as a single disk.
  \item May be able to take advantage of using two disks for reading.
  \end{itemize}
}

\frame
{
  What is the best use of 4 disks?

  Is RAID-6 and improvement over
  RAID-5?\footnote{\url{http://en.wikipedia.org/wiki/RAID}}
  \begin{itemize}
  \item RAID 5 (striped disks with parity) combines three or more disks
		in a way that protects data against loss of any one disk; the
		storage capacity of the array is reduced by one disk.
  \item RAID 6 (striped disks with dual parity) (less common) can
        recover from the loss of two disks.
  \end{itemize}

  Do the answers depend on what filesystem is used?
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-ext2}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-ext3}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-ext4}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-jfs}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-reiserfs}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{4-disk-xfs}
  \end{center}
}

\frame[containsverbatim]
{
  \frametitle{Filesystem Readahead}

  Values are in number of 512-byte segments, set per device.

  To get the current readahead size:
  \begin{verbatim}
$ blockdev --getra /dev/sda
256
  \end{verbatim}

  To set the readahead size to 8 MB:
  \begin{verbatim}
$ blockdev --setra 16384 /dev/sda
  \end{verbatim}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{readahead-throughput}
  \end{center}
}

\frame
{
  \frametitle{What does this all mean for my database system?}

  It depends...  Let's look at one sample of just using difference
  filesystems.
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{dbt2-fs}
  \end{center}
}

\frame
{
  \frametitle{Raw data for comparing filesystems}

  Initial inspection of results suggests we are close to being bound by
  processor and storage performance:

  \tiny
  \begin{itemize}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/ext2.1500.2/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/ext3.1500.2/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/ext4.1500.2/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/jfs.1500.2/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/reiserfs.1500.2/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-fs/xfs.1500.2/}
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Linux I/O Elevator Algorithm}

  To get current algorithm in []'s:
  \begin{verbatim}
$ cat /sys/block/sda/queue/scheduler 
noop anticipatory [deadline] cfq 
  \end{verbatim}
  
  To set a different algorithm:
  \begin{verbatim}
$ echo cfg > /sys/block/sda/queue/scheduler 
noop anticipatory [deadline] cfq 
  \end{verbatim}
  
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{dbt2-elevator}
  \end{center}
}

\frame
{
  \frametitle{Raw data for comparing I/O elevator algorithms}

  Initial inspection of results suggests we are close to being bound by
  processor and storage performance:

  \tiny
  \begin{itemize}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-elevator/ext2.anticipatory.1/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-elevator/ext2.cfs.1/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-elevator/ext2.deadline.1/}
    \item \url{http://207.173.203.223/~markwkm/community6/dbt2/m-elevator/ext2.noop.1/}
  \end{itemize}
}

\frame
{
  \frametitle{Why use deadline?\footnote{Linux source code:
    Documentation/block/as-iosched.txt}}

  \begin{itemize}
    \item Attention! Database servers, especially those using "TCQ"
	  disks should investigate performance with the 'deadline' IO
	  scheduler.  Any system with high disk performance requirements
	  should do so, in fact.
    \item Also, users with hardware RAID controllers, doing striping,
	  may find highly variable performance results with using the
	  as-iosched.  The as-iosched anticipatory implementation is
	  based on the notion that a disk device has only one physical
	  seeking head.  A striped RAID controller actually has a head
          for each physical device in the logical RAID device.
  \end{itemize}
}

\frame[containsverbatim]
{
  \frametitle{Bonus Data - Preview of FreeBSD}

  \footnotesize
  \color{red}
  \begin{verbatim}
             ,        ,         
            /(        )`        
            \ \___   / |        
            /- _  `-/  '        
           (/\/ \ \   /\        
           / /   | `    \       
           O O   ) /    |       
           `-^--'`<     '       
          (_.)  _  )   /        
           `.___/`    /         
             `-----' /          
<----.     __ / __   \          
<----|====O)))==) \) /====      
<----'    `--' `.__,' \         
             |        |         
              \       /       /\
         ______( (_  / \______/ 
       ,'  ,-----'   |          
       `--{__________)   
  \end{verbatim}

  \tiny
  \url{http://207.173.203.223/~markwkm/community10/freebsd-7.1/fio/2.6.28/}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{raid0-ufs}
  \end{center}
}

\frame
{
  \begin{center}
    \pgfimage[width=1\textwidth]{1-disk-raid0-os}
  \end{center}
}

\frame
{
  \frametitle{Final Notes}

  \begin{itemize}
    \item You have to test your own system to see what it can do.
  \end{itemize}
}

\frame
{
  \frametitle{Materials Are Freely Available - In a new location!}

  PDF
  \begin{itemize}
  \item \url{http://www.slideshare.net/markwkm}
  \end{itemize}

  \LaTeX{}~Beamer (source)
  \begin{itemize}
  \item \footnotesize
        \url{http://git.postgresql.org/git/performance-tuning.git}
  \end{itemize}
}

\frame
{
  \frametitle{Time and Location}

  When: 2nd Thursday of the month\\
  Location: Portland State University\\
  Room: FAB 86-01 (Fourth Avenue Building)\\
  Map: \url{http://www.pdx.edu/map.html}
}

\frame[containsverbatim]
{
  \frametitle{Coming up next time\ldots Effects of tuning PostgreSQL
  Parameters}

  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \  . o O ( Thank you! )
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame[containsverbatim]
{
  \frametitle{Acknowledgements}

  Haley Jane Wakenshaw\\
  \color{cyan}
  \begin{verbatim}
          __     __
         /  \~~~/  \
   ,----(     oo    )
  /      \__     __/
 /|         (\  |(
^ \   /___\  /\ |
   |__|   |__|-"
  \end{verbatim}
}

\frame
{
  \frametitle{License}

  This work is licensed under a Creative Commons Attribution 3.0
  Unported License. To view a copy of this license, (a) visit
  \url{http://creativecommons.org/licenses/by/3.0/us/}; or, (b) send a
  letter to Creative Commons, 171 2nd Street, Suite 300, San Francisco,
  California, 94105, USA.
}

\end{document}
