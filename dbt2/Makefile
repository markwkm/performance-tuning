all: \
	00-dbt2-introduction.pdf \
	01-dbt2-background.pdf \
	02-dbt2-workload-details.pdf \
	03-dbt2-howto.pdf \
	04-dbt2-linux-filesystems.pdf \
	05-dbt2-tuning.pdf \

%.png: %.svg
	inkscape -f $< -e $@

%.png: %.r
	R --no-save < $<

%.eps: %.jpg
	convert $< $@

%.eps: %.png
	convert $< $@

%.eps: %.tif
	convert $< $@

IMAGES00=cmdprompt.eps hp_logo.eps ibm1972.eps psulogo_horiz_msword.eps
IMAGES01=osdl.eps osdl_logo.eps dbt2-component-3.eps
IMAGES02=dbt2-schema.eps dbt2-component-1.eps dbt2-component-2.eps \
		dbt2-component-3.eps dbt2-payment.eps dbt2-new-order.eps \
		dbt2-delivery.eps dbt2-order-status.eps dbt2-stock-level.eps
IMAGES04=1-disk-raid0.eps 4-disk-raid0.eps mirror-ext2.eps 4-disk-ext2.eps \
		4-disk-ext3.eps 4-disk-ext4.eps 4-disk-jfs.eps 4-disk-reiserfs.eps \
		4-disk-xfs.eps readahead-throughput.eps rr-iostat-kb.eps \
		rw-iostat-kb.eps mix-iostat-kb.eps sr-iostat-kb.eps \
		sw-iostat-kb.eps dbt2-fs.eps raid0-ext2.eps raid0-ext3.eps \
		raid0-ext4.eps raid0-jfs.eps raid0-reiserfs.eps raid0-xfs.eps \
		dbt2-elevator.eps raid0-ufs.eps 1-disk-raid0-os.eps
IMAGES05=dbt2-sb.eps notpm.eps

00-dbt2-introduction.dvi: 00-dbt2-introduction.tex $(IMAGES00)
	-rm -f $@
	latex $<

01-dbt2-background.dvi: 01-dbt2-background.tex $(IMAGES01)
	-rm -f $@
	latex $<

02-dbt2-workload-details.dvi: 02-dbt2-workload-details.tex $(IMAGES02)
	-rm -f $@
	latex $<

03-dbt2-howto.dvi: 03-dbt2-howto.tex
	-rm $@
	latex $<

04-dbt2-linux-filesystems.dvi: 04-dbt2-linux-filesystems.tex $(IMAGES04)
	-rm $@
	latex $<

05-dbt2-tuning.dvi: 05-dbt2-tuning.tex $(IMAGES05)
	-rm $@
	latex $<

%.pdf: %.dvi
	dvipdf $<

clean:
	rm -f *-dbt2-*.log *-dbt2-*.toc *-dbt2-*.nav *-dbt2-*.snm *-dbt2-*.out \
			*-dbt2-*.dvi *-dbt2-*.aux *-dbt2-*.pdf *-dbt2-*.vrb texput.log \
			$(IMAGES00) $(IMAGES01) $(IMAGES02) $(IMAGES04) $(IMAGES05)
