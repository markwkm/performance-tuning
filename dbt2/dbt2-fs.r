d <- read.table("dbt2-fs.data", header = TRUE)
png("dbt2-fs.png", width = 1280, height = 800)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "1500 Warehouses",
	sub = "2.6.28-gentoo",
	ylab = "New Order Transactions per Minute",
	col = rainbow(1))
grid(nx = NA, ny = NULL)
dev.off()
