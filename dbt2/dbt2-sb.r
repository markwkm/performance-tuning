d <- read.table("dbt2-sb.data", header = TRUE)
png("dbt2-sb.png", width = 1280, height = 800)
color <- rainbow(1)
plot.default(d$shared_buffers, y = d$notpm, 
	type = "o",
	main = "1500 Warehouses",
	sub = "2.6.28-gentoo",
	xlab = "shared_buffers (GB)",
	ylab = "New Order Transactions per Minute",
    ylim = c(0, 13000)
	)
grid()
points(d$shared_buffers, d$notpm, type = "o", pch = 0, col = color[1])
dev.off()
