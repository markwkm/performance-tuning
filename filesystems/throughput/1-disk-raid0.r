d <- read.table("1-disk-raid0.data", header = TRUE, row.names = 1)
png("1-disk-raid0.png", width = 1280, height = 800)
color <- rainbow(2)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "1 Disk RAID 0",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("FreeBSD 7.1", "ext2 on Linux 2.6.28-gentoo"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
