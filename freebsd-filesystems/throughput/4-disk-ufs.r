d <- read.table("4-disk-ufs.data", header = TRUE, row.names = 1)
png("4-disk-ufs.png", width = 1280, height = 800)
color <- rainbow(4)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "ufs RAID 0",
	sub = "FreeBSD 7.1",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("RAID 0", "RAID 10", "RAID 5", "RAID 6"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
