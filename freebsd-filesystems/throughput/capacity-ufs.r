d <- read.table("capacity-ufs.data", header = TRUE, row.names = 1)
png("capacity-ufs.png", width = 1280, height = 800)
color <- rainbow(4)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "Comparing RAID Capacity on ufs",
	sub = "FreeBSD 7.1",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("2 Disk RAID 0", "3 Disk RAID 5", "4 Disk RAID 10", "4 Disk RAID 6"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
