d <- read.table("raid0-ufs.data", header = TRUE, row.names = 1)
png("raid0-ufs.png", width = 1280, height = 800)
color <- rainbow(4)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "ufs RAID 0",
	sub = "FreeBSD 7.1",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("1 Disk", "2 Disks", "3 Disks", "4 Disks"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
