d <- read.table("2-disk-raid0.data", header = TRUE, row.names = 1)
png("2-disk-raid0.png", width = 1280, height = 800)
color <- rainbow(6)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "2 Disk RAID 0",
	sub = "2.6.28-gentoo",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("ext2", "ext3", "ext4", "jfs", "reiserfs", "xfs"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
