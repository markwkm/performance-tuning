d <- read.table("3-disk-raid5.data", header = TRUE, row.names = 1)
png("3-disk-raid5.png", width = 1280, height = 800)
color <- rainbow(6)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "3 Disk RAID 5",
	sub = "2.6.28-gentoo",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("ext2", "ext3", "ext4", "jfs", "reiserfs", "xfs"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
