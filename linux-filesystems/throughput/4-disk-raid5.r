d <- read.table("4-disk-raid5.data", header = TRUE, row.names = 1)
png("4-disk-raid5.png", width = 1280, height = 800)
color <- rainbow(8)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "4 Disk RAID 5",
	sub = "2.6.28-gentoo",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("ext2", "ext3", "ext3-journal", "ext3-writeback", "ext4", "jfs", "reiserfs", "xfs"),
	bty = "n",
	fill = color)
grid(nx = NA, ny = NULL)
dev.off()
