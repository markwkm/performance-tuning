d <- read.table("capacity-jfs.data", header = TRUE, row.names = 1)
png("capacity-jfs.png", width = 1280, height = 800)
color <- rainbow(4)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "Comparing RAID Capacity on jfs",
	sub = "2.6.28-gentoo",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("2 Disk RAID 0", "3 Disk RAID 5", "4 Disk RAID 10", "4 Disk RAID 6"),
	bty = "n",
	fill = color);
grid(nx = NA, ny = NULL)
dev.off()
