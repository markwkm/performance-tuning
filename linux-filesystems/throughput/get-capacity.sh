#!/bin/sh

for fs in ext2 ext3 ext4 jfs reiserfs xfs; do
	#echo ${config}
	DATAFILE="capacity-${fs}.data"
	PNGFILE="capacity-${fs}.png"
	echo '"filesystem" "Random Read" "Random Write" "Read/Write (Reads)" "Read/Write (Writes)" "Sequential Read" "Sequential Write"' > ${DATAFILE}
for config in 2-disk-raid0 3-disk-raid5 4-disk-raid10 4-disk-raid6; do
		#echo ${fs}
		grep -H ${fs} ${config}.data >> ${DATAFILE}
	done

RFILE="capacity-${fs}.r"

echo "d <- read.table(\"${DATAFILE}\", header = TRUE, row.names = 1)" > ${RFILE}
echo "png(\"${PNGFILE}\", width = 1280, height = 800)" >> ${RFILE}
echo "color <- rainbow(4)" >> ${RFILE}
echo "barplot(as.matrix(d, rownames.force = TRUE)," >> ${RFILE}
echo "	beside = TRUE," >> ${RFILE}
echo "	main = \"Comparing RAID Capacity on ${fs}\"," >> ${RFILE}
echo "	sub = \"2.6.28-gentoo\"," >> ${RFILE}
echo "	ylab=\"Megabytes per Second\"," >> ${RFILE}
echo "	las = 1," >> ${RFILE}
echo "	col = color)" >> ${RFILE}
echo "legend(\"topleft\"," >> ${RFILE}
echo "	c(\"2 Disk RAID 0\", \"3 Disk RAID 5\", \"4 Disk RAID 10\", \"4 Disk RAID 6\")," >> ${RFILE}
echo "	bty = \"n\"," >> ${RFILE}
echo "	fill = color);" >> ${RFILE}
echo "grid(nx = NA, ny = NULL)" >> ${RFILE}
echo "dev.off()" >> ${RFILE}

done
