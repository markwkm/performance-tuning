d <- read.table("raid0-xfs.data", header = TRUE, row.names = 1)
png("raid0-xfs.png", width = 1280, height = 800)
color <- rainbow(3)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "xfs RAID 0",
	sub = "2.6.28-gentoo",
	ylab="Megabytes per Second",
	las = 1,
	col = color)
legend("topleft",
	c("1 Disk", "2 Disks", "4 Disks"),
	bty = "n",
	fill = color)
grid(nx = NA, ny = NULL)
dev.off()
