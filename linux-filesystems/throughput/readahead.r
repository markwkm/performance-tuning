d <- read.table("readahead.data", header = TRUE)
dy <- read.table("readahead.data", header = TRUE, row.names = 1)
max_x <- max(c(d$blocksize))
max_y <- max(dy, na.rm = TRUE)
png("readahead.png", width = 1280, height = 800)
color <- rainbow(6)
# Seems to be a bug when 'y' is not set for sizing the x-axis.
plot.default(d$blocksize, y = d$ext2,
	type = "o",
	main = "4 Disk RAID 0 Sequential Read Throughput",
	sub = "2.6.28-gentoo",
	xlab = "Readahead Buffer Size (KB)",
	ylab = "Megabytes per Second",
	xaxt = "n",
	log = "x",
	ylim = c(0, max_y))
axis(1, at = d$blocksize)
grid()
points(d$blocksize, d$ext2, type = "o", pch = 0, col = color[1])
points(d$blocksize, d$ext3, type = "o", pch = 1, col = color[2])
points(d$blocksize, d$ext4, type = "o", pch = 2, col = color[3])
points(d$blocksize, d$jfs, type = "o", pch = 3, col = color[4])
points(d$blocksize, d$reiserfs, type = "o", pch = 4, col = color[5])
points(d$blocksize, d$xfs, type = "o", pch = 5, col = color[6])
legend("topleft", names(dy), pch = 0:5, col = color)
dev.off()
