d <- read.table("4-disk-raid10.data", header = TRUE, row.names = 1)
png("4-disk-raid10.png", width = 1280, height = 800)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "4 Disk RAID 10",
	sub = "2.6.28-gentoo",
	ylab = "IO Operations per Second",
	las = 1,
	col = c("red", "orange", "green", "blue", "purple"))
legend("topright",
	c("Random Read", "Random Write", "Read/Write", "Sequential Read", "Sequential Write"),
	bty = "n",
	fill = c("red", "orange", "green", "blue", "purple"));
grid(nx = NA, ny = NULL)
dev.off()
