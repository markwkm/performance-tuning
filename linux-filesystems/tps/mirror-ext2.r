d <- read.table("mirror-ext2.data", header = TRUE, row.names = 1)
png("mirror-ext2.png", width = 1280, height = 800)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "ext2",
	sub = "2.6.28-gentoo",
	ylab = "IO Operations per Second",
	las = 1,
	col = c("red", "orange", "green", "blue", "purple"))
legend("topleft",
	c("Random Read", "Random Write", "Read/Write", "Sequential Read", "Sequential Write"),
	bty = "n",
	fill = c("red", "orange", "green", "blue", "purple"));
grid(nx = NA, ny = NULL)
dev.off()
