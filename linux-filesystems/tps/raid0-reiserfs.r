d <- read.table("raid0-reiserfs.data", header = TRUE, row.names = 1)
png("raid0-reiserfs.png", width = 1280, height = 800)
barplot(as.matrix(d, rownames.force = TRUE),
	beside = TRUE,
	main = "reiserfs RAID 0",
	sub = "2.6.28-gentoo",
	ylab = "IO Operations per Second",
	las = 1,
	col = c("red", "orange", "green", "blue", "purple"))
legend("topleft",
	c("Random Read", "Random Write", "Read/Write", "Sequential Read", "Sequential Write"),
	bty = "n",
	fill = c("red", "orange", "green", "blue", "purple"));
grid(nx = NA, ny = NULL)
dev.off()
